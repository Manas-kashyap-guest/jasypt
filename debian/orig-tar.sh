#!/bin/sh
set -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

rm $3
svn export http://svn.code.sf.net/p/jasypt/code/tags/jasypt/jasypt-$VERSION $DIR

XZ_OPT=--best tar -c -v -J -f $TAR $DIR
rm -Rf $DIR
